# LICENSE:
# Copyright (c) 2018, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Ben Farnham <ben.farnham@cern.ch>


#
# Include this file in your quasar server specific build configuration to obtain a clone of
# google's googletest in your source and included in the build.
# Your unit test code then requires to include the googletest headers and link with 
# the googletest binaries.
#

function ( clone_googletest GOOGLETEST_VERSION)
  if( EXISTS ${PROJECT_BINARY_DIR}/googletest)
    message(STATUS "function clone_googletest found an existing local googletest directory [${PROJECT_BINARY_DIR}/googletest].")
    message(STATUS "Assuming googletest is already cloned, returning without cloning the repo.")
    message(STATUS "To obtain a clean googletest clone delete this local directory and re-run the quasar prepare_build command.")
    return()
  endif()
  message(STATUS "cloning googletest from github. *NOTE* cloning [${GOOGLETEST_VERSION}]")
  execute_process(COMMAND git clone -b ${GOOGLETEST_VERSION} https://github.com/google/googletest.git WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  message(STATUS "googletest cloned to [${PROJECT_BINARY_DIR}/googletest]")    
endfunction()

clone_googletest("release-1.8.1")

set( BUILD_GTEST ON CACHE BOOL "We do want to build gtest")
set( BUILD_GMOCK OFF CACHE BOOL "we do not want to build gmock")
set (INSTALL_GTEST OFF CACHE BOOL "no need to install, googletest is embedded in the project")
set( gtest_force_shared_crt ON CACHE BOOL "use shared CRT" FORCE)

message(STATUS "adding directory googletest to build")
add_subdirectory( ${PROJECT_BINARY_DIR}/googletest ${PROJECT_BINARY_DIR}/googletest )
