/* � Copyright CERN, 2018.  All rights not expressly granted are reserved.
* AsioNetSnmpBridge.cpp
*
*  This file is part of Quasar.
*  Author: Ben Farnham
*
*  Quasar is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public Licence as published by
*  the Free Software Foundation, either version 3 of the Licence.
*
*  Quasar is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public Licence for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "AsioNetSnmpBridge.h"
#include <iostream>
#include <iomanip>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <LogIt.h>
#include <AsioNetSnmpBridgeLogItComponentIds.h>

using std::list;
using std::string;
using std::setfill;
using std::setw;

AsioNetSnmpBridge::AsioNetSnmpBridge(boost::asio::io_service& io_service)
:m_snmpSocket(io_service), m_snmpHandle(NULL) 
{
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "+";
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "-";
}

AsioNetSnmpBridge::~AsioNetSnmpBridge() 
{
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "+";
	snmp_sess_close(m_snmpHandle); 
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "-";
}

int asynchronousResponseCallback(int operation, struct snmp_session *sp, int reqid,
	struct snmp_pdu *pdu, void *magic)
{
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "+ operation [" << operation << "] snmp_session [0x" << std::hex << sp << std::dec << "] reqid [" << reqid << "] pdu type ["<<(pdu? AsioNetSnmpBridge::pduTypeToString(pdu->command):"NULL")<<"] magic [0x" << std::hex << magic << std::dec << "]";

	list<string> userHandlerArg;
	switch (operation)
	{
	case NETSNMP_CALLBACK_OP_RECEIVED_MESSAGE:
		{
			if (pdu->errstat == SNMP_ERR_NOERROR)
			{
				for( struct variable_list * currentVariable = pdu->variables; currentVariable != nullptr; currentVariable = currentVariable->next_variable )
				{
					char buf[1024];
					memset(buf, 0, 1024);
					snprint_variable(buf, 1024, currentVariable->name, currentVariable->name_length, currentVariable);
					LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< ": "<< buf;
					userHandlerArg.push_back(string(buf));
				}
			}
			else
			{
				LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " operation [" << operation << ", NETSNMP_CALLBACK_OP_RECEIVED_MESSAGE] but some error present - stat ["<<pdu->errstat<<"]";
				userHandlerArg.push_back("error occurred");
			}
		}
		break;
	case NETSNMP_CALLBACK_OP_TIMED_OUT:
		LOG(Log::WRN, SNMP_BRIDGE)<<__FUNCTION__<< " TIMED_OUT";
		userHandlerArg.push_back("timed out");
		break;
	case NETSNMP_CALLBACK_OP_SEND_FAILED:
		LOG(Log::WRN, SNMP_BRIDGE)<<__FUNCTION__<< " SEND_FAILED";
		userHandlerArg.push_back("send failed");
		break;
	case NETSNMP_CALLBACK_OP_CONNECT:
		LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< " CONNECT";
		userHandlerArg.push_back("connection callback");
		break;
	case NETSNMP_CALLBACK_OP_DISCONNECT:
		LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< " DISCONNECT";
		userHandlerArg.push_back("disconnection callback");
		break;
	default:
		LOG(Log::ERR, SNMP_BRIDGE)<<__FUNCTION__<< " No handler for unknown operation [" << operation << "]";
		userHandlerArg.push_back("unknown operation");
		break;
	}

	if (magic)
	{
		SNMPHandler* userHandler = static_cast<SNMPHandler*>(magic);
		(*userHandler)(userHandlerArg);
	}

	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< "-";
	return 1;
}

bool AsioNetSnmpBridge::initMib(const std::string& mib)
{
	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< " mib [" << mib << "]";
	if (mib.empty()) return true; // nothing to do

	init_mib();
	if (!read_module(mib.c_str()))
	{
		LOG(Log::ERR, SNMP_BRIDGE)<<__FUNCTION__<< " failed to load mib ["<<mib<<"].";
		return false;
	}

	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< " loaded mib ["<<mib<<"]";
	return true;
}

bool AsioNetSnmpBridge::connect(const string& peername, const string& community, const std::string& mib/*empty*/, const std::string& applicationName /*="AsioNetSnmpBridge"*/)
{
	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< "+ version [SNMP_VERSION_2c] peername ["<<peername<<"] community ["<<community<<"] mib ["<<mib<<"] applicationName ["<<applicationName<<"]";

	/* initialize library */
	init_snmp(netsnmp_strdup(applicationName.c_str()));
	if (!initMib(mib)) return false;

    struct snmp_session sess;
    snmp_sess_init(&sess);   /* initialize session */
    sess.version = SNMP_VERSION_2c;
	sess.peername = netsnmp_strdup(peername.c_str());
    sess.community = reinterpret_cast<u_char*>(netsnmp_strdup(community.c_str()));
    sess.community_len = community.size();
    sess.callback = asynchronousResponseCallback;

	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< " opening session...";
	m_snmpHandle = snmp_sess_open(&sess);
	if (!m_snmpHandle)
	{
		LOG(Log::ERR, SNMP_BRIDGE) << __FUNCTION__<< " Failed to initialise a net-snmp session, snmp_sess_open returned NULL handle";
		return false;
	}
	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< " session opened, returned [0x"<<std::hex<< m_snmpHandle <<std::dec<<"]";

	// pass SNMP session socket to boost asio.
    netsnmp_transport *transport  = snmp_sess_transport(m_snmpHandle);
	if (!transport)
	{
		LOG(Log::ERR, SNMP_BRIDGE) << __FUNCTION__<< " Failed to obtain SNMP session socket for handle [0x" << std::hex << m_snmpHandle << std::dec << "]";
		return false;
	}
	m_snmpSocket.assign(boost::asio::ip::udp::v4(), transport->sock);
    
	LOG(Log::INF, SNMP_BRIDGE)<<__FUNCTION__<< "- is socket in non-blocking mode? ["<<(m_snmpSocket.non_blocking())<<"]";
	return true;
}

void AsioNetSnmpBridge::sendDataUsingSnmp(const boost::system::error_code &errorCode, std::size_t bytesTransferred, struct snmp_pdu *pdu, SNMPHandler* responseHandler)
{
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "+";

	if (!m_snmpSocket.is_open())
	{
		LOG(Log::ERR, SNMP_BRIDGE)<<__FUNCTION__<< ": socket closed. No data will be sent!";
		return;
	}
	if (errorCode) 
	{
		LOG(Log::ERR, SNMP_BRIDGE)<<__FUNCTION__<< ": error code ["<<errorCode.message()<<"]. No data will be sent!";
		return;
	}

	// Notify net-snmp library that it can perform a write. 
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " calling snmp_sess_async_send for pdu type ["<<AsioNetSnmpBridge::pduTypeToString(pdu->command)<<"]";
	const int requestId = snmp_sess_async_send(m_snmpHandle, pdu, asynchronousResponseCallback, responseHandler);
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " called snmp_sess_async_send, returned requestId ["<<requestId<<"]";

	// level of abstraction - call internal callback to process commands before calling user-space callback.
	auto recvHandler = boost::bind(&AsioNetSnmpBridge::recvDataUsingSnmp, this);

	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " calling socket async_receive...";
	m_snmpSocket.async_receive(boost::asio::null_buffers(), recvHandler);
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " called socket async_receive.";

	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "-";
}

void AsioNetSnmpBridge::recvDataUsingSnmp()
{
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "+";
	if (!m_snmpSocket.is_open())
	{
		LOG(Log::ERR, SNMP_BRIDGE)<<__FUNCTION__<< ": socket closed!";
		return;
	}

	int fds = 0, block = 1;
	fd_set fdset;
	FD_ZERO(&fdset);
	struct timeval timeout;
	memset(&timeout, 0, sizeof(struct timeval));
	int result = snmp_sess_select_info_flags(m_snmpHandle, &fds, &fdset, &timeout, &block, NETSNMP_SELECT_NOFLAGS);
	LOG(Log::TRC, SNMP_BRIDGE) << __FUNCTION__ << " snmp_sess_select_info_flags returned ["<<result<<"] block ["<<block<<"] timeout  [" << timeout.tv_sec << "." << setfill('0') << setw(6) << timeout.tv_usec << "]";
	
	// investigate - create new fdset obtained during open (i.e. from m_snmpHandle somehow, check net-snmp impl. ?).
	const int selectResult = select(fds, &fdset, NULL, NULL, block ? NULL : &timeout);
	if (selectResult > 0)
	{
		LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " select detected ["<<selectResult<<"] fd(s) ready for reading, calling snmp_sess_read...";
		snmp_sess_read(m_snmpHandle, &fdset);
		LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " snmp_sess_read called.";
	}
	else if (selectResult == 0)
	{
		LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " select timed out (timeout was set to ["<<timeout.tv_sec<<"."<<setfill('0')<<setw(6)<<timeout.tv_usec<<"])";
		snmp_timeout();
	}
	else if (selectResult < 0)
	{
		LOG(Log::ERR, SNMP_BRIDGE)<<__FUNCTION__<< " select failed, result ["<<selectResult<<"]";
	}

	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "-";
}

struct snmp_pdu* createSnmpPdu(const AsioNetSnmpBridge::PduVariables& pduVariables, const int& pduType, const std::string& community)
{
	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "+ pduVariables count [" << pduVariables.size() << "] pduType ["<< AsioNetSnmpBridge::pduTypeToString(pduType)<<"] community ["<<community<<"]";

	struct snmp_pdu* pdu = snmp_pdu_create(pduType);
	pdu->community = (u_char*)netsnmp_strdup(community.c_str());
	pdu->community_len = strlen((char*)pdu->community);

	for (AsioNetSnmpBridge::PduVariable pduVariable : pduVariables)
	{
		const string stringOid = std::get<0>(pduVariable);
		
		size_t snmpOidLen = MAX_OID_LEN;
		oid snmpOid[MAX_OID_LEN];
		memset(snmpOid, 0, sizeof(oid)*MAX_OID_LEN);

		if (!snmp_parse_oid(stringOid.c_str(), snmpOid, &snmpOidLen))
		{
			snmp_perror(stringOid.c_str());
			LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " failed to parse OID [" << stringOid << "] - this oid will be omitted from the pdu";
			continue;
		}

		switch (pdu->command)
		{
		case SNMP_MSG_GET:
			snmp_add_null_var(pdu, snmpOid, snmpOidLen);
			break;
		case SNMP_MSG_SET:
			snmp_add_var(pdu, snmpOid, snmpOidLen, std::get<1>(pduVariable), netsnmp_strdup(std::get<2>(pduVariable).c_str()));
			break;
		default:
			LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< " failed to parse message type [" << pduType << "] - this oid will be omitted from the pdu";
			continue;
		}
	}

	LOG(Log::TRC, SNMP_BRIDGE)<<__FUNCTION__<< "-";
	return pdu;
}

void AsioNetSnmpBridge::asyncReadOid(const PduVariables& pduVariables, SNMPHandler* readResponseHandler)
{
	LOG(Log::DBG, SNMP_BRIDGE)<<__FUNCTION__<< "+ pduVariables count ["<<pduVariables.size()<<"]";
	struct snmp_pdu* pdu = createSnmpPdu(pduVariables, SNMP_MSG_GET, "public");

	// asio socket delegates to sendDataUsingSnmp to send (which calls snmp_sess_async_send)
	auto sendHandler = boost::bind(
		&AsioNetSnmpBridge::sendDataUsingSnmp, this, // -> method
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, pdu, readResponseHandler // -> args
	);

	LOG(Log::DBG, SNMP_BRIDGE)<<__FUNCTION__<< " calling asio async_send";
	m_snmpSocket.async_send(boost::asio::null_buffers(), sendHandler);
	LOG(Log::DBG, SNMP_BRIDGE) << __FUNCTION__<< "-";
}

void AsioNetSnmpBridge::asyncWriteOid(const PduVariables& pduVariables, SNMPHandler* writeResponseHandler)
{
	LOG(Log::DBG, SNMP_BRIDGE)<<__FUNCTION__<< "+  pduVariables count [" << pduVariables.size() << "]";
	struct snmp_pdu* pdu = createSnmpPdu(pduVariables, SNMP_MSG_SET, "guru");

	// asio socket delegates to sendDataUsingSnmp to send (which calls snmp_sess_async_send)
	auto sendHandler = boost::bind(
		&AsioNetSnmpBridge::sendDataUsingSnmp, this, // -> method
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, pdu, writeResponseHandler // -> args
	);

	LOG(Log::DBG, SNMP_BRIDGE)<<__FUNCTION__<< " calling asio async_send";
	m_snmpSocket.async_send(boost::asio::null_buffers(), sendHandler);
	LOG(Log::DBG, SNMP_BRIDGE)<<__FUNCTION__<< "-";
}

string AsioNetSnmpBridge::pduTypeToString(const int pduType)
{
	switch (pduType)
	{
	case SNMP_MSG_SET: return "SNMP_MSG_SET";
	case SNMP_MSG_GET: return "SNMP_MSG_GET";
	default: return "UNKNOWN";
	}
}
