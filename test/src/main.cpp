/* � Copyright CERN, 2018.  All rights not expressly granted are reserved.
* main.cpp
*
*  This file is part of Quasar.
*  Author: Ben Farnham
*
*  Quasar is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public Licence as published by
*  the Free Software Foundation, either version 3 of the Licence.
*
*  Quasar is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public Licence for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include "gtest/gtest.h"
#include <LogIt.h>
#include <AsioNetSnmpBridgeLogItComponentIds.h>

extern Log::LogComponentHandle SNMP_BRIDGE = Log::INVALID_HANDLE;

int main(int argc, char** argv)
{
	std::cout << "running boost-asio-net-snmp unit tests" << std::endl;

	Log::initializeLogging();
	SNMP_BRIDGE = Log::registerLoggingComponent("SNMP_BRIDGE", Log::TRC);
	LOG(Log::INF, SNMP_BRIDGE) << "Hello log world, from the SNMP_BRIDGE unit tests";

	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
