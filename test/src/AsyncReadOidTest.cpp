/* � Copyright CERN, 2018.  All rights not expressly granted are reserved.
* AsioReadOidTest.cpp
*
*  This file is part of Quasar.
*  Author: Ben Farnham
*
*  Quasar is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public Licence as published by
*  the Free Software Foundation, either version 3 of the Licence.
*
*  Quasar is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public Licence for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "AsyncReadOidTest.h"
#include "gtest/gtest.h"
#include <iostream>
#include <thread>
#include <chrono>
#include <tuple>
#include <boost/asio.hpp>

using std::cout;
using std::endl;
using std::list;
using std::string;
using std::ostringstream;
using std::make_tuple;

AsyncReadOidTest::AsyncReadOidTest()
{}

AsyncReadOidTest::~AsyncReadOidTest()
{}

void handleReadNotification(list<string> msgs)
{
	cout <<__FUNCTION__<< " called:";
	for (const string msg : msgs)
	{
		cout<<endl<<"\t -> " << msg;
	}
	cout << endl;
}
SNMPHandler g_sTestReadHandler(handleReadNotification);

void handleWriteNotification(list<string> msgs)
{
	cout << __FUNCTION__ << " called:";
	for (const string msg : msgs)
	{
		cout << endl << "\t -> " << msg;
	}
	cout << endl;
}
SNMPHandler g_sTestWriteHandler(handleWriteNotification);

TEST_F(AsyncReadOidTest, testRead)
{
	boost::asio::io_service asioService;
	AsioNetSnmpBridge testee(asioService);
	EXPECT_TRUE(testee.connect("pl512test01", "public", "WIENER-CRATE-MIB", "AsyncReadOidTest"));

	testee.asyncReadOid(AsioNetSnmpBridge::PduVariables{ make_tuple("powersupply.psOperatingTime.0", -1, "") }, &g_sTestReadHandler);
	testee.asyncReadOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputStatus.u0", -1, "") }, &g_sTestReadHandler);
	testee.asyncReadOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", -1, "") }, &g_sTestReadHandler);

	asioService.run();
}

TEST_F(AsyncReadOidTest, testWrite)
{
	boost::asio::io_service asioService;
	AsioNetSnmpBridge testee(asioService);
	EXPECT_TRUE(testee.connect("pl512test01", "public", "WIENER-CRATE-MIB", "AsyncReadOidTest"));

	testee.asyncWriteOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", 'F', "2.1") }, &g_sTestWriteHandler);
	testee.asyncReadOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", -1, "") }, &g_sTestReadHandler);

	testee.asyncWriteOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", 'F', "2.0") }, &g_sTestWriteHandler);
	testee.asyncReadOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", -1, "") }, &g_sTestReadHandler);

	testee.asyncWriteOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", 'F', "1.9") }, &g_sTestWriteHandler);
	testee.asyncReadOid(AsioNetSnmpBridge::PduVariables{ make_tuple("outputVoltage.u0", -1, "") }, &g_sTestReadHandler);

	asioService.run();
}

TEST_F(AsyncReadOidTest, testReadMultipleItemsSimultaneously)
{
	boost::asio::io_service asioService;
	AsioNetSnmpBridge testee(asioService);
	EXPECT_TRUE(testee.connect("pl512test01", "public", "WIENER-CRATE-MIB", "AsyncReadOidTest"));

	AsioNetSnmpBridge::PduVariables toRead{
		make_tuple("powersupply.psOperatingTime.0", -1, ""),
		make_tuple("outputStatus.u0", -1, ""),
		make_tuple("outputVoltage.u0", -1, "")
	};
	testee.asyncReadOid(toRead, &g_sTestReadHandler);

	asioService.run();
}
