/* � Copyright CERN, 2018.  All rights not expressly granted are reserved.
* AsioNetSnmpBridge.h
*
*  This file is part of Quasar.
*  Author: Ben Farnham
*
*  Quasar is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public Licence as published by
*  the Free Software Foundation, either version 3 of the Licence.
*
*  Quasar is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public Licence for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <string>
#include <tuple>
#include <list>
#include <functional>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

typedef std::function<void( std::list<std::string> )> SNMPHandler;

class AsioNetSnmpBridge
{
public:
		/*
		* PduVariable tuple: oid, value type, value string
		* Note! for asyncReadOid only the oid is used.
		*/
		typedef std::tuple < std::string, int, std::string > PduVariable;
		typedef std::list< PduVariable > PduVariables;

        AsioNetSnmpBridge(boost::asio::io_service& io_service);
        ~AsioNetSnmpBridge();
        
        bool connect(const std::string& peername, const std::string& community, const std::string& mib="", const std::string& applicationName="AsioNetSnmpBridge");
        void asyncReadOid(const PduVariables& pduVariables, SNMPHandler* readResponseHandler);
        void asyncWriteOid(const PduVariables& pduVariables, SNMPHandler* writeResponseHandler);
		static std::string pduTypeToString(const int pduType);
    private:
		void recvDataUsingSnmp();
		void sendDataUsingSnmp(const boost::system::error_code &errorCode, std::size_t bytesTransferred, struct snmp_pdu *pdu, SNMPHandler* responseHandler);
		bool initMib(const std::string& mib);

        boost::asio::ip::udp::socket m_snmpSocket;
        void *m_snmpHandle;
};
