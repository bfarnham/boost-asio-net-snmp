# boost-asio-net-snmp

Prototype component for wrapping net-snmp in a boost-asio structure

The boost-asio-net-snmp module uses [LogIt](https://github.com/quasar-team/LogIt) as its logging library. If you're building the boost-asio-net-snmp as part of a quasar server build, then boost-asio-net-snmp will pick up its LogIt dependency via the quasar build. Lucky you.

However, if you're using the boost-asio-net-snmp module outwith a quasar build - as a stand alone libray for example, then there is no quasar build to satisfy the LogIt dependency. Don't worry; this setup only requires providing 2 additional command line parameters to the cmake invocation (one to locate LogIt includes, the other to locate the LogIt binary). These variables are
```
-DLOGIT_INCLUDE_DIR
-DLOGIT_EXT_LIB_DIR
```

Note that this implies you already have a LogIt binary somwhere. Perhaps you haven't - then follow the instructions for building LogIt [here](https://github.com/quasar-team/LogIt) (build LogIt as a shared library).

_Run cmake generator on windows (run in build dir boost-asio-net-snmp-build-debug - references back to source dir ../boost-asio-net-snmp)_
```
cmake -DCMAKE_BUILD_TYPE=Debug \
-DNETSNMP_EXT_LIB_DIR=/d/3rdPartySoftware/net-snmp/5.7.3/build-debug/lib/ \
-DNETSNMP_INCLUDE_DIR=/d/3rdPartySoftware/net-snmp/5.7.3/src/include/ \
-DCMAKE_TOOLCHAIN_FILE=boost_custom_win_VS2017.cmake \
-G "Visual Studio 15 2017 Win64" \
../boost-asio-net-snmp
```

_Run cmake generator on linux (same directory structure as above)_
```
export BOOST_HOME=/local/bfarnham/workspace/boost_mapped_namespace_builder/work/MAPPED_NAMESPACE_INSTALL/64bit/
export BOOST_PATH_HEADERS=$BOOST_HOME/include
export BOOST_PATH_LIBS=$BOOST_HOME/lib

cmake -DCMAKE_TOOLCHAIN_FILE=boost_custom_cc7.cmake ../boost-asio-net-snmp
```